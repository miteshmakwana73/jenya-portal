package com.jenya.jenyaportal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jenya.jenyaportal.jsonurl.Config;

public class MainActivity extends AppCompatActivity {
    WebView mywebview;

    String HttpUrl = Config.WEBVIEW;
    int count=0;

    private ProgressBar progressBar;
    LinearLayout lnstatus;
    TextView status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mywebview = (WebView) findViewById(R.id.webView1);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        status=(TextView)findViewById(R.id.tvstatus);
        lnstatus=(LinearLayout) findViewById(R.id.lnstatus);

        checkconnection();

    }

    private void checkconnection() {
//        status.setVisibility(View.GONE);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {

            lnstatus.setVisibility(View.GONE);
            loadjenyaportal();

        }else {
            //no connection
            count=1;
            lnstatus.setVisibility(View.VISIBLE);
//            status.setText(R.string.noconnection);
//            Toast.makeText(this, "No Connection Found", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadjenyaportal() {
        progressBar.setVisibility(View.VISIBLE);
        mywebview.getSettings().setJavaScriptEnabled(true);

        WebSettings webSettings = mywebview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDomStorageEnabled(true);

        mywebview.setWebViewClient(new WebViewController());

//      data="<html><body><h1>Hello, Javatpoint!</h1></body></html>";
//	    mywebview.loadData(data, "text/html", "UTF-8");
//      mywebview.loadData(data, "text/html; charset=UTF-8", null);

        mywebview.loadUrl(HttpUrl);
    }

    public class WebViewController extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_reload:

                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                android.net.NetworkInfo wifi = cm
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                android.net.NetworkInfo datac = cm
                        .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                if ((wifi != null & datac != null)
                        && (wifi.isConnected() | datac.isConnected())) {
                    if(count==1)
                    {
                        checkconnection();
                        count=0;
                    }
                    else
                    {
                        mywebview.loadUrl(mywebview.getUrl());
                        progressBar.setVisibility(View.VISIBLE);
                        lnstatus.setVisibility(View.GONE);
                    }


                }else {
                    lnstatus.setVisibility(View.VISIBLE);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
